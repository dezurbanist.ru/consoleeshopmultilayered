﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
	public interface IMenuService
	{
		/// <summary>
		/// Get all accessible menu-commands by the user
		/// </summary>
		/// <param name="user">Current User(DTO)</param>
		/// <returns>List of all accessible commands by the user</returns>
		IEnumerable<MenuCommandDTO> GetMenuCmndsByUser(UserDTO user);
		/// <summary>
		/// Get all accessible menu-commands by guest
		/// </summary>
		/// <returns>List of all accessible commands by guest</returns>
		IEnumerable<MenuCommandDTO> GetBasicGuestMenu();

		/// <summary>
		/// Check accessibility the menu-command
		/// </summary>
		/// <param name="key">Keyboard code</param>
		/// <returns></returns>
		//bool CheckAccessibilityCmnd(string key);


	}
}
