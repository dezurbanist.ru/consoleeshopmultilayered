﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories.Interfaces;
using DBEntity = ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.BLL.Services
{
	public class UserService : IUserService
	{
		const string DEFAULT_USER_ROLE = "Customer";
		readonly IRepositoryWrapper _repositorie;
		readonly IMapper _mapper;

		public UserService(IRepositoryWrapper repositoryWrapper, IMapper mapper)
		{
			_repositorie = repositoryWrapper;
			_mapper = mapper;
		}

		public UserDTO CreateUser(string login, string password, string firstName, string lastName)
		{
			UserDTO user = new UserDTO();
			user.Id = _repositorie.User.GetNewID();
			user.Login = login.ToLower();
			user.Password = password.ToLower();
			user.FirstName = firstName;
			user.LastName = lastName;
			user.Role = _mapper.Map<DBEntity.UserRole, UserRoleDTO>(_repositorie.UserRole.Get(ur => ur.Name == DEFAULT_USER_ROLE));
			user.RoleId = user.Role.Id;
			
			var dbUser = _mapper.Map<UserDTO, DBEntity.User>(user);
			_repositorie.User.Create(dbUser);
			return user;
		}

		public bool CheckUniqueLogin(string login)
		{
			if (_repositorie.User.Get(u => u.Login == login.ToLower()) == null)
			{
				return true;
			}
			return false;
		}

		public void ChangeUserProfile(UserDTO user)
		{
			var dbUser = _mapper.Map<UserDTO, DBEntity.User>(user);
			_repositorie.User.Update(dbUser);
		}

		public UserDTO FindUserByLogin(string login)
		{
			var user = _repositorie.User.Get(u => u.Login == login.ToLower());
			if (user == null)
			{
				return null;
			}
			return _mapper.Map<DBEntity.User, UserDTO>(user);
		}

		public IEnumerable<UserDTO> GetAllUsers()
		{
			var users = _repositorie.User.GetAll();
			return _mapper.Map<IEnumerable<DBEntity.User>, IEnumerable<UserDTO>>(users);
		}

		public void DeleteUser(UserDTO user)
		{
			var dbUser = _mapper.Map<UserDTO, DBEntity.User>(user);
			_repositorie.User.Delete(dbUser);
		}
	}
}
