﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
	public class UserRole : IEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public ICollection<MenuCommand> Commands { get; set; }

		public UserRole()
		{

		}
		public UserRole(int id, string name, IEnumerable<MenuCommand> commands)
		{
			Id = id;
			Name = name;
			Commands = commands.ToList();
		}
	}
}
