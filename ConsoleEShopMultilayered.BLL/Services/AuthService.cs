﻿using System;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories.Interfaces;
using DBEntity = ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.BLL.Services
{
	public class AuthService : IAuthService
	{
		readonly IRepositoryWrapper _repositorie;
		readonly IMapper _mapper;

		public AuthService(IRepositoryWrapper repositoryWrapper, IMapper mapper)
		{
			_repositorie = repositoryWrapper;
			_mapper = mapper;
		}
		

		public UserDTO SignIn(string login, string password)
		{
			var user = _repositorie.User.Get(u => u.Login == login.ToLower());
			if(user == null)
			{
				return null;
			}
			if(password != user.Password)
			{
				return null;
			}
			return _mapper.Map<DBEntity.User, UserDTO>(user);
		}

		
	}
}
