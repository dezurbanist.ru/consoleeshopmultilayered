﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.DAL.Repositories.Realizations
{
	public class ProductRepository : RepositoryBase<Product>
	{
		public ProductRepository(EShopContext context) : base(context)
		{ }
	}
}
