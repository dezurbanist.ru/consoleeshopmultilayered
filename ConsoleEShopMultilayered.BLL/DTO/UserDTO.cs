﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
	public class UserDTO
	{
		public int Id { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int RoleId { get; set; }
		public UserRoleDTO Role { get; set; }
		public string Phone { get; set; }
		public ICollection<ProductDTO> Cart { get; set; }
		public ICollection<OrderDTO> Orders { get; set; }
		public UserDTO()
		{
			Cart = new List<ProductDTO>();
			Orders = new List<OrderDTO>();
		}
		public UserDTO(int id, string login, string password, string firstName, string lastName, UserRoleDTO role)
		{
			Id = id;
			Login = login;
			Password = password;
			FirstName = firstName;
			LastName = lastName;
			RoleId = role.Id;
			Role = role;
			Cart = new List<ProductDTO>();
			Orders = new List<OrderDTO>();
		}
		public override string ToString()
		{
			return $"Имя:{FirstName}, Фамилия:{LastName}, Телефон:{Phone}";
		}
	}
}
