﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
	public class User : IEntity
	{
		public int Id { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int RoleId { get; set; }
		public UserRole Role { get; set; }
		public string Phone { get; set; }
		public ICollection<Product> Cart { get; set; }
		public ICollection<Order> Orders { get; set; }

		public User()
		{
			Cart = new List<Product>();
			Orders = new List<Order>();
		}
		public User(int id, string login, string password, string firstName, string lastName, UserRole role)
		{
			Id = id;
			Login = login;
			Password = password;
			FirstName = firstName;
			LastName = lastName;
			RoleId = role.Id;
			Role = role;
			Cart = new List<Product>();
			Orders = new List<Order>();
		}
	}
}
