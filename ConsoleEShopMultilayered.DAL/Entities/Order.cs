﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
	public class Order : IEntity
	{
		public int Id { get; set; }
		public int Number { get; set; }
		public int OrderStatusId { get; set; }
		public OrderStatus OrderStatus { get; set; }
		public ICollection<Product> Products { get; set; }
		public int UserId { get; set; }
		public User User { get; set; }

		public Order()
		{

		}
		public Order(int id, int number, OrderStatus status, IEnumerable<Product> product, User user)
		{
			Id = id;
			Number = number;
			OrderStatusId = status.Id;
			OrderStatus = status;
			Products = product.ToList();
			UserId = user.Id;
			User = user;
		}
	}
}
