﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.DAL.Repositories.Realizations
{
	public class OrderStatusRepository : RepositoryBase<OrderStatus>
	{
		public OrderStatusRepository(EShopContext context) : base(context)
		{ }
	}
}
