﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
	public interface IProductService
	{
		/// <summary>
		/// Create new product
		/// </summary>
		/// <param name="product">An information about new product</param>
		/// <returns></returns>
		void CreateProduct(ProductDTO product);

		/// <summary>
		/// Find a product
		/// </summary>
		/// <param name="name">The name of product</param>
		/// <returns>Product(DTO) which was found</returns>
		ProductDTO FindProductByName(string name);

		/// <summary>
		/// Get all products
		/// </summary>
		/// <returns>List of all products</returns>
		IEnumerable<ProductDTO> GetAllProducts();

		/// <summary>
		/// Change the product
		/// </summary>
		/// <param name="product">Product(DTO) which needs to be changed</param>
		/// <returns></returns>
		void ChangeProduct(ProductDTO product);
	}
}
