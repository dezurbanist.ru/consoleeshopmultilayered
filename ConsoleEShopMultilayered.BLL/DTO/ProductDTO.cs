﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
	public class ProductDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public int CategoryId { get; set; }
		public CategoryDTO Category { get; set; }

		public ProductDTO()
		{

		}
		public ProductDTO(int id, string name, string description, decimal price, CategoryDTO category)
		{
			Id = id;
			Name = name;
			Description = description;
			Price = price;
			CategoryId = category.Id;
			Category = category;
		}
		public override string ToString()
		{
			return Name + "  |  " + Category.Name + "  |  " + Description + "  |  " + Price;
		}
	}
}
