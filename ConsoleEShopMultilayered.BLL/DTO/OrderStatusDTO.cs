﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
	public class OrderStatusDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public OrderStatusDTO()
		{

		}
		public OrderStatusDTO(int id, string name)
		{
			Id = id;
			Name = name;
		}
		public override string ToString()
		{
			return Name;
		}
	}
}
