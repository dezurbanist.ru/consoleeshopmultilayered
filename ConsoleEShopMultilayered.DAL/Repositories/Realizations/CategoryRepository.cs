﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.DAL.Repositories.Realizations
{
	public class CategoryRepository : RepositoryBase<Category>
	{
		public CategoryRepository(EShopContext context) : base(context)
		{ }
	}
}
