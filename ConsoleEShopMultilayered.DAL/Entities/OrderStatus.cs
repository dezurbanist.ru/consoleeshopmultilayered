﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
	public class OrderStatus : IEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public OrderStatus()
		{

		}
		public OrderStatus(int id, string name)
		{
			Id = id;
			Name = name;
		}
	}
}
