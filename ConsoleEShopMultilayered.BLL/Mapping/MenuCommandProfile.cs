﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using DBEntities = ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.BLL.Mapping
{
	public class MenuCommandProfile : Profile
	{
		public MenuCommandProfile()
		{
			CreateMap<DBEntities.MenuCommand, MenuCommandDTO>().ReverseMap();
		}
	}
}
