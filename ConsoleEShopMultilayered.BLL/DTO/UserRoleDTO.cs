﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
	public class UserRoleDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public ICollection<MenuCommandDTO> Commands { get; set; }

		public UserRoleDTO()
		{

		}
		public UserRoleDTO(int id, string name, IEnumerable<MenuCommandDTO> commands)
		{
			Id = id;
			Name = name;
			Commands = commands.ToList();
		}
		public override string ToString()
		{
			return Name;
		}
	}
}
