﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.DAL.Repositories.Realizations
{
	public class UserRoleRepository : RepositoryBase<UserRole>
	{
		public UserRoleRepository(EShopContext context) : base(context)
		{ }
	}
}
