﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
	public class OrderDTO
	{
		public int Id { get; set; }
		public int Number { get; set; }
		public int OrderStatusId { get; set; }
		public OrderStatusDTO OrderStatus { get; set; }
		public ICollection<ProductDTO> Products { get; set; }
		public decimal TotalSum { get => (Products.Count > 0) ? Products.Sum(p=>p.Price) : 0 ; }
		public int UserId { get; set; }
		public UserDTO User { get; set; }

		public OrderDTO()
		{
			Products = new List<ProductDTO>();
		}
		public OrderDTO(int id, int number, OrderStatusDTO status, IEnumerable<ProductDTO> product, UserDTO user)
		{
			Id = id;
			Number = number;
			OrderStatusId = status.Id;
			OrderStatus = status;
			Products = product.ToList();
			UserId = user.Id;
			User = user;
		}
		public override string ToString()
		{
			return $"Номер заказа:{Number} | Сумма: {TotalSum} |  Состояние:{OrderStatus.ToString()}";
		}
	}
}
