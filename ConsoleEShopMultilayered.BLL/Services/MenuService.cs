﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories.Interfaces;
using DBEntity = ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.BLL.Services
{
	public class MenuService : IMenuService
	{
		readonly IRepositoryWrapper _repositorie;
		readonly IMapper _mapper;

		public MenuService(IRepositoryWrapper repositoryWrapper, IMapper mapper)
		{
			_repositorie = repositoryWrapper;
			_mapper = mapper;
		}
		//public bool CheckAccessibilityCmnd(string key)
		//{
		//	throw new NotImplementedException();
		//}

		public IEnumerable<MenuCommandDTO> GetMenuCmndsByUser(UserDTO user)
		{
			var menuCommands = _repositorie.UserRole.Get(ur => ur.Id == user.Role.Id).Commands.ToList();
			return _mapper.Map<IEnumerable<DBEntity.MenuCommand>, IEnumerable<MenuCommandDTO>>(menuCommands);
		}
		public IEnumerable<MenuCommandDTO> GetBasicGuestMenu()
		{
			var menuCommands = _repositorie.UserRole.Get(1).Commands.ToList();
			return _mapper.Map<IEnumerable<DBEntity.MenuCommand>, IEnumerable<MenuCommandDTO>>(menuCommands);
		}
	}
}
