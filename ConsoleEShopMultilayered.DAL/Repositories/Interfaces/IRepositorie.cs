﻿using ConsoleEShopMultilayered.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
	public interface IRepositorie<TEntity> where TEntity : class, IEntity
	{
		void Create(TEntity item);
		void Delete(TEntity item);
		IEnumerable<TEntity> GetAll();
		IEnumerable<TEntity> GetAll(Func<TEntity, bool> predicate);
		TEntity Get(int id);
		TEntity Get(Func<TEntity, bool> predicate);
	}
}
