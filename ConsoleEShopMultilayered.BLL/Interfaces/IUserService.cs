﻿using System;
using System.Collections.Generic;
using ConsoleEShopMultilayered.BLL.DTO;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
	public interface IUserService
	{
		/// <summary>
		/// Create a new user
		/// </summary>
		/// <param name="login">User login</param>
		/// <param name="password">User password</param>
		/// <param name="firstName">User firstName</param>
		/// <param name="lastName">User lastName</param>
		UserDTO CreateUser(string login, string password, string firstName, string lastName);

		/// <summary>
		/// Check unique of login
		/// </summary>
		/// <param name="login">Account login</param>
		/// <returns>Return true if login is unique, overwise false</returns>
		bool CheckUniqueLogin(string login);

		/// <summary>
		/// Get all users
		/// </summary>
		/// <returns></returns>
		IEnumerable<UserDTO> GetAllUsers();

		/// <summary>
		/// Find the user
		/// </summary>
		/// <param name="login">User login</param>
		/// <returns>User(DTO) which was found</returns>
		UserDTO FindUserByLogin(string login);

		/// <summary>
		/// Change the user profile
		/// </summary>
		/// <param name="user">User(DTO) which needs to be changed</param>
		/// <returns></returns>
		void ChangeUserProfile(UserDTO user);

		/// <summary>
		/// Delete the user
		/// </summary>
		/// <param name="user">User(DTO) which needs to be deleted</param>
		/// <returns></returns>
		void DeleteUser(UserDTO user);
	}
}
