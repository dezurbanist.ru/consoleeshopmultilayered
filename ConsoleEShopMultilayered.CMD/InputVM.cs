﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.CMD
{
	public class InputVM
	{
		public string Message { get; set; }
		public ICollection<string> SubNav { get; set; }
		public InputVM()
		{
			SubNav = new List<string>();
		}
	}
}
