﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
	public interface IOrderService
	{
		/// <summary>
		/// Create new order
		/// </summary>
		/// <param name="user">Current User(DTO)</param>
		/// <returns></returns>
		OrderDTO CreateOrder(UserDTO user);

		/// <summary>
		/// Change status of order 
		/// </summary>
		/// <param name="order">Order(DTO) which needs to be changed</param>
		/// <param name="statusId">The ID of new status</param>
		/// <returns></returns>
		void ChangeOrderStatus(OrderDTO order, int statusId);

		/// <summary>
		/// Get all orders by user
		/// </summary>
		/// <param name="user">Current User(DTO)</param>
		/// <returns>List of all orders</returns>
		IEnumerable<OrderDTO> GetOrdersByUser(UserDTO user);

		/// <summary>
		/// Get all order statuses
		/// </summary>
		/// <returns>List of all order statuses</returns>
		IEnumerable<OrderStatusDTO> GetAllOrderStatuses();
	}
}
