﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories.Interfaces;
using DBEntity = ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.BLL.Services
{
	public class ProductService : IProductService
	{
		readonly IRepositoryWrapper _repositorie;
		readonly IMapper _mapper;

		public ProductService(IRepositoryWrapper repositoryWrapper, IMapper mapper)
		{
			_repositorie = repositoryWrapper;
			_mapper = mapper;
		}
		public void ChangeProduct(ProductDTO product)
		{
			var dbProduct = _mapper.Map<ProductDTO, DBEntity.Product>(product);
			_repositorie.Product.Update(dbProduct);
		}

		public void CreateProduct(ProductDTO product)
		{
			product.Id = _repositorie.Product.GetNewID();
			var dbProduct = _mapper.Map<ProductDTO, DBEntity.Product>(product);
			_repositorie.Product.Create(dbProduct);
		}

		public ProductDTO FindProductByName(string name)
		{
			var product = _repositorie.Product.Get(p => p.Name.ToLower().Contains(name.ToLower()));
			if (product == null)
			{
				return null;
			}
			return _mapper.Map<DBEntity.Product, ProductDTO>(product);
		}

		public IEnumerable<ProductDTO> GetAllProducts()
		{
			var products = _repositorie.Product.GetAll();
			return _mapper.Map<IEnumerable<DBEntity.Product>, IEnumerable<ProductDTO>>(products);
		}
	}
}
