﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories.Interfaces;
using DBEntity = ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.BLL.Services
{
	public class CategoryService : ICategoryService
	{
		readonly IRepositoryWrapper _repositorie;
		readonly IMapper _mapper;

		public CategoryService(IRepositoryWrapper repositoryWrapper, IMapper mapper)
		{
			_repositorie = repositoryWrapper;
			_mapper = mapper;
		}
		public IEnumerable<CategoryDTO> GetAllCategories()
		{
			var categories = _repositorie.Category.GetAll();
			return _mapper.Map<IEnumerable<DBEntity.Category>, IEnumerable<CategoryDTO>>(categories);
		}

		public CategoryDTO GetCategoryById(int id)
		{
			var category = _repositorie.Category.Get(id);
			if(category == null)
			{
				return null;
			}
			return _mapper.Map<DBEntity.Category, CategoryDTO>(category);
		}
	}
}
