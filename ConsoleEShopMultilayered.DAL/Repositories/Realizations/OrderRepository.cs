﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.DAL.Repositories.Realizations
{
	public class OrderRepository : RepositoryBase<Order>
	{
		public OrderRepository(EShopContext context) : base(context)
		{ }
	}
}
