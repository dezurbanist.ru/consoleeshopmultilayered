﻿using ConsoleEShopMultilayered.CMD.Controllers;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace ConsoleEShopMultilayered.CMD
{
   class Program
   {
      static string header = "";
      static ICollection<string> nav = null;
      static ICollection<string> body = null;
      static string message = null;
      static void Main(string[] args)
      {
         IServiceProvider _serviceProvider = Extension.AddServices().BuildServiceProvider();
         Controller controller = _serviceProvider.GetService<Controller>();
         controller.NotifyViewHandler += ViewHandler;
         controller.NotifyInputHandler += InputHandler;
         controller.NotifyMessageHandler += showMessage;
         controller.GetStartUI();
         
         while (true)
         {
            Console.Clear();
            Console.WriteLine(header);
            Console.WriteLine();
            foreach (string n in nav)
            {
               Console.WriteLine(n);
            }
            
            Console.WriteLine(new string('-', 100));

            if (body != null)
            {
               foreach (var item in body)
               {
                  Console.WriteLine(item);
               }
            }

            if (message != null)
				{
               Console.WriteLine();
               Console.WriteLine(message);
               message = null;
            }

            controller.PushCommand(Console.ReadKey().Key);
         }
      }


      static void ViewHandler(ConsoleVM model)
		{
         if(model.Header != null)
			{
            header = model.Header;
         }
			if(model.Nav.Count > 0)
			{
            nav = model.Nav;
         }
			if (model.Body == null)
			{
            body = null;
            return;
			}
			if(model.Body.Count > 0)
			{
            body = model.Body;
         }
		}

      static string InputHandler(InputVM model)
      {
         if (model.SubNav.Count == 0)
         {
            Console.WriteLine(model.Message);
            return Console.ReadLine();
         }
         else
         {
            Console.Clear();
            foreach (string n in model.SubNav)
				{
               Console.WriteLine(n);
            }
            Console.WriteLine("Введите номер команды:");
            var key = Console.ReadKey().Key.ToString();
            Console.Clear();
            return key;
         }
      }
      static void showMessage(string messge)
      {
         message = messge;
      }
   }
}

