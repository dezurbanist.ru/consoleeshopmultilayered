﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using DBEntities = ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.BLL.Mapping
{
	public class OrderProfile : Profile
	{
		public OrderProfile()
		{
			CreateMap<DBEntities.Order, OrderDTO>().ReverseMap();
		}
	}
}
