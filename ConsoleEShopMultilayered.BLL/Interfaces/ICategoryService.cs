﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
	public interface ICategoryService
	{
		/// <summary>
		/// Get category
		/// </summary>
		/// <param name="id">The Id of the category</param>
		/// <returns></returns>
		CategoryDTO GetCategoryById(int id);

		/// <summary>
		/// Get all categories
		/// </summary>
		/// <returns>List of product categories</returns>
		IEnumerable<CategoryDTO> GetAllCategories();
	}
}
