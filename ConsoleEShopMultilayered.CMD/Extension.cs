﻿using AutoMapper;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.BLL.Services;
using ConsoleEShopMultilayered.CMD.Controllers;
using ConsoleEShopMultilayered.DAL;
using ConsoleEShopMultilayered.DAL.Repositories.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories.Realizations;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace ConsoleEShopMultilayered.CMD
{
	public static class Extension
	{
		public static IServiceCollection AddServices()
		{
			var services = new ServiceCollection();
			services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies()
					.Where(x => x.FullName.Equals("ConsoleEShopMultilayered.BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")));
			services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
			services.AddScoped<IUserService, UserService>();
			services.AddScoped<IProductService, ProductService>();
			services.AddScoped<IAuthService, AuthService>();
			services.AddScoped<ICategoryService, CategoryService>();
			services.AddScoped<IMenuService, MenuService>();
			services.AddScoped<IOrderService, OrderService>();
			services.AddScoped<Controller>();

			return services;
		}
	}
}
