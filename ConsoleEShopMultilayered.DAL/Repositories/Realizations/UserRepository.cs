﻿using ConsoleEShopMultilayered.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Repositories.Realizations
{
	public class UserRepository : RepositoryBase<User>
	{
		public UserRepository(EShopContext context) : base(context)
		{}
	}
}
