﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Repositories.Interfaces;

namespace ConsoleEShopMultilayered.DAL.Repositories.Realizations
{
	public class RepositoryWrapper : IRepositoryWrapper
	{
		private readonly EShopContext _context;
		private UserRepository _user;
		private UserRoleRepository _userRole;
		private ProductRepository _product;
		private OrderRepository _order;
		private OrderStatusRepository _orderStatus;
		private CategoryRepository _category;

		public UserRepository User
		{
			get
			{
				if (_user == null)
				{
					_user = new UserRepository(_context);
				}
				return _user;
			}
		}

		public UserRoleRepository UserRole
		{
			get
			{
				if (_userRole == null)
				{
					_userRole = new UserRoleRepository(_context);
				}
				return _userRole;
			}
		}

		public ProductRepository Product
		{
			get
			{
				if (_product == null)
				{
					_product = new ProductRepository(_context);
				}
				return _product;
			}
		}

		public OrderRepository Order
		{
			get
			{
				if (_order == null)
				{
					_order = new OrderRepository(_context);
				}
				return _order;
			}
		}

		public OrderStatusRepository OrderStatus
		{
			get
			{
				if (_orderStatus == null)
				{
					_orderStatus = new OrderStatusRepository(_context);
				}
				return _orderStatus;
			}
		}

		public CategoryRepository Category
		{
			get
			{
				if (_category == null)
				{
					_category = new CategoryRepository(_context);
				}
				return _category;
			}
		}

		
		public RepositoryWrapper()
		{
			_context = EShopContext.GetContext();
		}
	}
}
