﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.BLL.DTO;
using System.Text;

namespace ConsoleEShopMultilayered.CMD.Controllers
{
	public class Controller
	{
		private UserDTO currentUser;
		public delegate void ViewHandler(ConsoleVM model);
		public event ViewHandler NotifyViewHandler;

		public delegate string InputHandler(InputVM model);
		public event InputHandler NotifyInputHandler;

		public delegate void MessageHandler(string message);
		public event MessageHandler NotifyMessageHandler;

		readonly IAuthService _authService;
		readonly IUserService _userService;
		readonly IProductService _productService;
		readonly IOrderService _orderService;
		readonly ICategoryService _categoryService;
		readonly IMenuService _menuService;

		public Controller(
			IAuthService authService,
			IUserService userService,
			IProductService productService,
			IOrderService orderService,
			ICategoryService categoryService,
			IMenuService menuService)
		{
			_authService = authService;
			_userService = userService;
			_productService = productService;
			_orderService = orderService;
			_categoryService = categoryService;
			_menuService = menuService;
		}


		public void PushCommand(ConsoleKey key)
		{
			if (currentUser == null)
			{
				if (_menuService.GetBasicGuestMenu().All(ms => ms.Key != key.ToString()))
					NotifyMessageHandler.Invoke("Вы ввели неизвестную команду");
				else
					ExecuteCommand(key);
			}
			else if (_menuService.GetMenuCmndsByUser(currentUser).Any(ms => ms.Key == key.ToString()))
			{
				ExecuteCommand(key);
			}
			else
			{
				NotifyMessageHandler.Invoke("Вы ввели неизвестную команду");
			}
		}
		public void GetStartUI()
		{
			ConsoleVM model = new ConsoleVM();
			List<string> guestNav = new List<string>();
			foreach (var nav in _menuService.GetBasicGuestMenu())
			{
				guestNav.Add(nav.ToString());
			}
			model.Header = CreateConsoleVMHeader();
			model.Nav = guestNav;
			model.Body = null;
			NotifyViewHandler.Invoke(model);
		}

		void ShowAllProducts()
		{
			List<string> products = new List<string>();
			foreach (var product in _productService.GetAllProducts())
			{
				products.Add(product.ToString());
			}
			NotifyViewHandler.Invoke(new ConsoleVM { Body = products });
		}

		void SearchProduct()
		{
			string productName = NotifyInputHandler.Invoke(new InputVM { Message = "Введите название товара:" }).ToLower();

			var product = _productService.FindProductByName(productName)?.ToString();
			if (product == null)
			{
				NotifyMessageHandler.Invoke("Товара с таким название не существует");
				return;
			}

			ConsoleVM model = new ConsoleVM();
			model.Body.Add(product);
			NotifyViewHandler.Invoke(model);
		}

		void SignIn()
		{
			string login = NotifyInputHandler.Invoke(new InputVM { Message = "Введите логин:" }).Trim().ToLower();
			string password = NotifyInputHandler.Invoke(new InputVM { Message = "Введите пароль:" }).Trim().ToLower();
			if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
			{
				NotifyMessageHandler.Invoke("Вы не ввели требуемого значения");
				return;
			}

			var user = _authService.SignIn(login, password);
			if (user == null)
			{
				NotifyMessageHandler.Invoke("Учетной записи с таким логином или паролем не существует");
				return;
			}
			currentUser = user;
			ConsoleVM model = new ConsoleVM();
			model.Header = CreateConsoleVMHeader();
			foreach (var nav in _menuService.GetMenuCmndsByUser(user))
			{
				model.Nav.Add(nav.ToString());
			}

			NotifyViewHandler.Invoke(model);
		}

		void Registration()
		{
			string login = NotifyInputHandler.Invoke(new InputVM { Message = "Введите логин:" }).Trim().ToLower();
			if (!_userService.CheckUniqueLogin(login))
			{
				NotifyMessageHandler.Invoke("Такой логин уже существует, придумайте другой");
				return;
			}
			string password = NotifyInputHandler.Invoke(new InputVM { Message = "Введите пароль:" }).Trim().ToLower();
			string firstName = NotifyInputHandler.Invoke(new InputVM { Message = "Введите Имя:" }).Trim();
			string lastName = NotifyInputHandler.Invoke(new InputVM { Message = "Введите Фамилию:" }).Trim();

			if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
			{
				NotifyMessageHandler.Invoke("Вы не ввели требуемого значения");
				return;
			}
			_userService.CreateUser(login, password, firstName, lastName);
			NotifyMessageHandler.Invoke("Используйте свои логин и пароль для входа");
		}

		void AddToCart()
		{
			string productName = NotifyInputHandler.Invoke(new InputVM { Message = "Введите название товара:" }).Trim().ToLower();
			if (string.IsNullOrEmpty(productName))
			{
				NotifyMessageHandler.Invoke("Вы не ввели требуемого значения");
				return;
			}
			var product = _productService.FindProductByName(productName);
			if (product == null)
			{
				NotifyMessageHandler.Invoke("Товара с таким название не существует");
				return;
			}
			currentUser.Cart.Add(product);
			NotifyViewHandler.Invoke(new ConsoleVM { Header = CreateConsoleVMHeader() });
		}

		void CreateOrder()
		{
			if(currentUser.Cart.Count == 0)
			{
				NotifyMessageHandler.Invoke("В корзине нет товаров");
				return;
			}
			if(currentUser.Phone == null)
			{
				currentUser.Phone = NotifyInputHandler.Invoke(new InputVM { Message = "Введите свой телефон:" }).ToLower();
			}
			
			currentUser.Orders.Add(_orderService.CreateOrder(currentUser));
			currentUser.Cart.Clear();
			NotifyViewHandler.Invoke(new ConsoleVM { Header = CreateConsoleVMHeader() });
			NotifyMessageHandler.Invoke("Заказ создан, откройте историю заказов");
		}

		void ShowOrderHistory()
		{
			var orders = _orderService.GetOrdersByUser(currentUser).ToList();
			if (orders.Count == 0)
			{
				NotifyMessageHandler.Invoke("У Вас нет пока ни одного заказа");
				return;
			}
			currentUser.Orders = orders;
			ConsoleVM model = new ConsoleVM();
			foreach(var order in orders)
			{
				model.Body.Add(order.ToString());
			}
			NotifyViewHandler.Invoke(model);
		}

		void EditOrderStatus()
		{
			ShowOrderHistory();
			Int32.TryParse(NotifyInputHandler.Invoke(new InputVM { Message = "Введите номер заказа:" }), out int orderNumber);
			var order = currentUser.Orders.FirstOrDefault(o => o.Number == orderNumber);
			if(order == null)
			{
				NotifyMessageHandler.Invoke("Заказа с таким номером не существует");
				return;
			}
			
			StringBuilder msg = new StringBuilder("Введите номер статуса:");
			foreach (var status in _orderService.GetAllOrderStatuses())
			{
				msg.Append($" {status.Id} - {status.Name} |");
			}

			Int32.TryParse( NotifyInputHandler.Invoke(new InputVM { Message = msg.ToString() }) , out int newStatusId);
			_orderService.ChangeOrderStatus(order, newStatusId);
			
			ConsoleVM model2 = new ConsoleVM();
			foreach (var ordr in currentUser.Orders)
			{
				model2.Body.Add(ordr.ToString());
			}
			NotifyViewHandler.Invoke(model2);
		}

		void EditUserProfile(string key)
		{
			var subNav = _menuService.GetMenuCmndsByUser(currentUser).First(m => m.Key == key).SubMenuCommands;
			InputVM model = new InputVM();
			
			foreach (var nav in subNav)
			{
				model.SubNav.Add(nav.Value);
			}
			string newValue ="";
			switch (NotifyInputHandler.Invoke(model))
			{
				case "D1":
					newValue = NotifyInputHandler.Invoke(new InputVM { Message = "Введите новое имя:" }).Trim();
					if (!string.IsNullOrEmpty(newValue))
					{
						currentUser.FirstName = newValue;
					}
					break;
				case "D2":
					newValue = NotifyInputHandler.Invoke(new InputVM { Message = "Введите новую фамилию" }).Trim();
					if (!string.IsNullOrEmpty(newValue))
					{
						currentUser.LastName = newValue;
					}
					break;
				case "D0":
					return;
			}
			_userService.ChangeUserProfile(currentUser);
			NotifyViewHandler.Invoke(new ConsoleVM { Header = CreateConsoleVMHeader() });
		}

		void LogOut()
		{
			currentUser = null;
			GetStartUI();
		}

		void CreateProduct(string name, string description, decimal price, int categoryId)
		{
			throw new NotImplementedException();
		}

		void EditProduct(string name, string description, decimal price, int categoryId)
		{
			throw new NotImplementedException();
		}

		void ShowAllUsers()
		{
			throw new NotImplementedException();
		}

		void ExecuteCommand(ConsoleKey key)
		{
			switch (key)
			{
				case ConsoleKey.F1:
					ShowAllProducts();
					break;
				case ConsoleKey.F2:
					SearchProduct();
					break;
				case ConsoleKey.F3:
					SignIn();
					break;
				case ConsoleKey.F4:
					Registration();
					break;
				case ConsoleKey.F5:
					AddToCart();
					break;
				case ConsoleKey.F6:
					CreateOrder();
					break;
				case ConsoleKey.F7:
					ShowOrderHistory();
					break;
				case ConsoleKey.F8:
					EditOrderStatus();
					break;
				case ConsoleKey.F9:
					EditUserProfile(key.ToString());
					break;
				case ConsoleKey.D0:
					LogOut();
					break;
				case ConsoleKey.D1:
					//CreateProduct(model);
					break;
				case ConsoleKey.D2:
					//EditProduct(model);
					break;
				case ConsoleKey.D3:
					ShowAllUsers();
					break;
				default:
					Console.WriteLine("Команда не cуществует!");
					break;
			}
		}
		string CreateConsoleVMHeader()
		{
			if(currentUser == null)
			{
				return "Здравствуй - Guest | В корзине - 0 товаров на сумму 0.0$";
			}
			return $"Здравствуй {currentUser.FirstName} {currentUser.LastName} | В корзине - {currentUser.Cart.Count} товаров на сумму {currentUser.Cart.Sum(p => p.Price)}$";
		}
	}
}
