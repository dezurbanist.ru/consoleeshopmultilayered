﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
	public class Product : IEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public int CategoryId { get; set; }
		public Category Category { get; set; }

		public Product()
		{

		}
		public Product(int id, string name, string description, decimal price, Category category)
		{
			Id = id;
			Name = name;
			Description = description;
			Price = price;
			CategoryId = category.Id;
			Category = category;
		}
	}
}
