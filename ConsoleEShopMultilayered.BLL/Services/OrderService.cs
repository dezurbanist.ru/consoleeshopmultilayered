﻿using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories.Interfaces;
using DBEntity = ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.BLL.Services
{
	public class OrderService : IOrderService
	{
		const string DEFAULT_ORDER_STATUS = "Новый";
		readonly IRepositoryWrapper _repositorie;
		readonly IMapper _mapper;

		public OrderService(IRepositoryWrapper repositoryWrapper, IMapper mapper)
		{
			_repositorie = repositoryWrapper;
			_mapper = mapper;
		}
		public void ChangeOrderStatus(OrderDTO order, int statusId)
		{
			var orderStatus = _repositorie.OrderStatus.Get(statusId);
			if(order != null && orderStatus != null)
			{
				order.OrderStatus = _mapper.Map<DBEntity.OrderStatus, OrderStatusDTO>(orderStatus);
				order.OrderStatusId = orderStatus.Id;
				var dbOrder = _mapper.Map<OrderDTO, DBEntity.Order>(order);
				_repositorie.Order.Update(dbOrder);
			}
		}

		public OrderDTO CreateOrder(UserDTO user)
		{
			OrderDTO order = new OrderDTO();
			order.Id = _repositorie.Order.GetNewID();
			order.Number = _repositorie.Order.GetAll(o => o.UserId == user.Id).Count() + 1;
			order.OrderStatus = _mapper.Map<DBEntity.OrderStatus, OrderStatusDTO>(_repositorie.OrderStatus.Get(os => os.Name == DEFAULT_ORDER_STATUS));
			order.OrderStatusId = order.OrderStatus.Id;
			order.Products = user.Cart;
			order.User = user;
			order.UserId = user.Id;
			var dbOrder = _mapper.Map<OrderDTO, DBEntity.Order>(order);
			_repositorie.Order.Create(dbOrder);
			return order;
		}

		public IEnumerable<OrderDTO> GetOrdersByUser(UserDTO user)
		{
			var orders = _repositorie.Order.GetAll(o => o.UserId == user.Id);
			return _mapper.Map<IEnumerable<DBEntity.Order>, IEnumerable<OrderDTO>>(orders);
		}

		public IEnumerable<OrderStatusDTO> GetAllOrderStatuses()
		{
			var statuses = _repositorie.OrderStatus.GetAll();
			return _mapper.Map<IEnumerable<DBEntity.OrderStatus>, IEnumerable<OrderStatusDTO>>(statuses);
		}
	}
}
