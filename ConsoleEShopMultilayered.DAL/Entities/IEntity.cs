﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
	public interface IEntity
	{
		public int Id { get; set; }
	}
}
