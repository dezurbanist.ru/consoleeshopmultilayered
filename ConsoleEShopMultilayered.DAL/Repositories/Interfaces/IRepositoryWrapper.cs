﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Repositories.Realizations;

namespace ConsoleEShopMultilayered.DAL.Repositories.Interfaces
{
	public interface IRepositoryWrapper
	{
		public UserRepository User { get; }
		public ProductRepository Product { get; }
		public OrderRepository Order { get; }
		public OrderStatusRepository OrderStatus { get; }
		public CategoryRepository Category { get; }
		public UserRoleRepository UserRole { get; }
	}
}
