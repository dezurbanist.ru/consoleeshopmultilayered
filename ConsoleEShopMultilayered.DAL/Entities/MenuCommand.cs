﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
	public class MenuCommand : IEntity
	{
		public int Id { get; set; }
		public string Key { get; set; }
		public string Name { get; set; }
		public IDictionary<string, string> SubMenuCommands { get; set; }

		public MenuCommand()
		{

		}
		public MenuCommand(int id, string key, string name, IDictionary<string, string> subMenuCmnds = null)
		{
			Id = id;
			Key = key;
			Name = name;
			SubMenuCommands = subMenuCmnds;
		}
	}
}
