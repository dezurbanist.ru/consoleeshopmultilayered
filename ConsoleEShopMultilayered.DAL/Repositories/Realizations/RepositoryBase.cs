﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopMultilayered.DAL.Entities;
using System.Linq.Expressions;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
	public abstract class RepositoryBase<TEntity> : IRepositorie<TEntity> where TEntity : class, IEntity
	{
		private readonly EShopContext _context;

		protected ICollection<TEntity> dbSet;
		
		protected RepositoryBase(EShopContext context)
		{
			_context = context;
			dbSet = _context.Set<TEntity>();
		}
		public void Create(TEntity item)
		{
			dbSet.Add(item);
		}

		public void Delete(TEntity item)
		{
			dbSet.Remove(Get(item.Id));
		}

		public void Update(TEntity item)
		{
			Delete(item);
			dbSet.Add(item);
		}

		public IEnumerable<TEntity> GetAll()
		{
			return dbSet.ToList();
		}

		public IEnumerable<TEntity> GetAll(Func<TEntity, bool> predicate)
		{
			return dbSet.Where(predicate).ToList();
		}
		public TEntity Get(int id)
		{
			return dbSet.FirstOrDefault(e => e.Id == id);
		}
		public TEntity Get(Func<TEntity, bool> predicate)
		{
			return dbSet.FirstOrDefault(predicate);
		}
		public int GetNewID()
		{
			return dbSet.Max(e => e.Id) + 1;
		}
	}
}
