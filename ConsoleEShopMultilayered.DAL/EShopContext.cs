﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopMultilayered.DAL.Entities;
using System.Collections;

namespace ConsoleEShopMultilayered.DAL
{
	public class EShopContext
	{
		private static EShopContext context;

		private readonly ArrayList entityCollections = new ArrayList();
		public ICollection<User> Users { get; set; }
		public ICollection<OrderStatus> OrderStatuses { get; }
		public ICollection<Order> Orders { get; set; }
		public ICollection<Product> Products { get; set; }
		public ICollection<Category> Categories { get; set; }
		public ICollection<UserRole> UserRoles { get; set; }
		public ICollection<MenuCommand> Commands { get; set; }

		private EShopContext()
		{
			Commands = new List<MenuCommand>
			{
				new MenuCommand(1, "F1", "F1 - Все товары"),
				new MenuCommand(2, "F2", "F2 - Поиск товара"),
				new MenuCommand(3, "F3", "F3 - Авторизация"),
				new MenuCommand(4, "F4", "F4 - Регистрация"),
				new MenuCommand(5, "F5", "F5 - Добавить в корзину"),
				new MenuCommand(6, "F6", "F6 - Оформить заказ"),
				new MenuCommand(7, "F7", "F7 - История заказов"),
				new MenuCommand(8, "F8", "F8 - Изменить статус заказа"),
				new MenuCommand(9, "F9", "F9 - Изменить профиль", new Dictionary<string, string>{ { "D1", "1 - Изменить имя" }, { "D2", "2 - Изменить фамилию" }, { "D0", "0 - Назад к меню" } }),
				new MenuCommand(10, "D0", "0 - Выход с акк"),
				new MenuCommand(11, "D1", "1 - Создать товар"),
				new MenuCommand(12, "D2", "2 - Изменить товар", new Dictionary<string, string>{ { "D1", "1 - Изменить название" }, { "D2", "2 - Изменить цену" }, { "D3", "3 - Изменить описание" }, { "D0", "0 - Назад к меню" } }),
				new MenuCommand(13, "D3", "3 - Все пользователи", new Dictionary<string, string>{ { "D1", "1 - Удалить пользователя" }, { "D2", "2 - Изменить права доступа" }, { "D0", "0 - Назад к меню" } })
			};
			UserRoles = new List<UserRole>
			{
				new UserRole(1, "Guest", Commands.Where(c => c.Id < 5)),
				new UserRole(2, "Customer", Commands.Where(c => c.Id < 11 && c.Id != 3 && c.Id != 4)),
				new UserRole(3, "Admin", Commands.Where(c => c.Id != 3 && c.Id != 4))
			};
			Users = new List<User>
			{
				new User(1, "alex", "1111", "Alex", "Alex", UserRoles.First(ur => ur.Name == "Admin")),
				new User(2, "serg", "1234", "Don", "Sergio", UserRoles.First(ur => ur.Name == "Customer"))
			};
			OrderStatuses = new List<OrderStatus>
			{
				new OrderStatus(1, "Новый"),
				new OrderStatus(2, "Отменен"),
				new OrderStatus(3, "Оплачено"),
				new OrderStatus(4, "Доставлено"),
				new OrderStatus(5, "Получено"),
				new OrderStatus(6, "Завершено")
			};
			Categories = new List<Category>
			{
				new Category(1, "Monitors"),
				new Category(2, "Processors"),
				new Category(3, "Phones")
			};
			Products = new List<Product>
			{
				new Product(1, "Монитор LG 24'", "LG bla bla bla", 700, Categories.First(c => c.Name == "Monitors")),
				new Product(2, "Процессор Intel i7", "Intel bla bla bla", 410, Categories.First(c => c.Name == "Processors")),
				new Product(3, "Смартфон Xiaomi MI 10", "Xiaomi bla bla bla", 520, Categories.First(c => c.Name == "Phones")),
				new Product(4, "Смартфон Iphone", "Iphone bla bla bla", 820, Categories.First(c => c.Name == "Phones")),
			};
			Orders = new List<Order>
			{
				new Order(1, 1, OrderStatuses.First(os => os.Id == 6), Products.Where(p => p.Id == 2), Users.First(u => u.Login == "serg"))
			};

			entityCollections.Add(Users);
			entityCollections.Add(OrderStatuses);
			entityCollections.Add(Orders);
			entityCollections.Add(Products);
			entityCollections.Add(Categories);
			entityCollections.Add(UserRoles);
		}
		public static EShopContext GetContext()
		{
			if (context == null)
				context = new EShopContext();
			return context;
		}
		public ICollection<T> Set<T>() where T : class
		{
			foreach (var item in entityCollections)
			{
				if (item is ICollection<T>)
				{
					return item as ICollection<T>;
				}
			}
			throw new ArgumentException("The generic collection of this type doesn`t exist");
		}
		
	}
}
