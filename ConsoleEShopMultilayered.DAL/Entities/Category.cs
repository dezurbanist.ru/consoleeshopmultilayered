﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
	public class Category : IEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public Category()
		{

		}
		public Category(int id, string name)
		{
			Id = id;
			Name = name;
		}
	}
}
