﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
	public interface IAuthService
	{
		/// <summary>
		/// Authorization
		/// </summary>
		/// <param name="login">Account login</param>
		/// <param name="password">Account password</param>
		/// <returns>User(DTO) who was authorized</returns>
		UserDTO SignIn(string login, string password);
		
	}
}
