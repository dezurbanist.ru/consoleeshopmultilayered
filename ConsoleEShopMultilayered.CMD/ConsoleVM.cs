﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.CMD
{
	public class ConsoleVM
	{
		public string Header { get; set; }
		public ICollection<string> Nav { get; set; }
		public ICollection<string> Body { get; set; }
		public ConsoleVM()
		{
			Nav = new List<string>();
			Body = new List<string>();
		}
	}
}
